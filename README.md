ide64-seek-loader
=================

`ide64-seek-loader` is an [IDE64](http://www.ide64.org) software serving the purpose of a file archiver bundled together with an accompanying loading procedure. It takes a list of files on the input and produces a loader engine binary with a corresponding archive file bundling all of the input files into a single one. A result program is capable of seeking and loading arbitrary files from a concatenated archive file based on their index position, which is provided to a loader procedure as a single byte input value. The loader sets carry flag upon error, and assigns an original file loading address to X and Y register values.

VERSION
-------

Version 0.01 (2018-02-25)

INSTALLATION
------------

Build loading procedure and create an archive combining all of the input files into a single one:

    $ ./ide64-seek-loader.pl <START_ADDRESS> <LOADER_PRG> <DATA_PRG> <INPUT_FILES>
    $ ./ide64-seek-loader.pl 0x0e00 loader.prg data,prg desolate-deev.aas frighthof83-yazoo.fcp metal.kla niemanazwy-bimber.hpi

Include and use it within your own program's source code:

    ;----------------------------------------------
    IDE64_SEEK_LOADER_START_ADDRESS = $0e00
    ;----------------------------------------------
    setup       = IDE64_SEEK_LOADER_START_ADDRESS+0
    loader      = IDE64_SEEK_LOADER_START_ADDRESS+3
    ;----------------------------------------------
                ; ...
    ;----------------------------------------------
                lda #datafile_length
                ldx #<datafile
                ldy #>datafile
                jsr setup
    ;----------------------------------------------
                ; ...
    ;----------------------------------------------
                ldx #$00  ; index of a file to load
                jsr loader  ; ("desolate-deev.aas")
                bcs error
    ;----------------------------------------------
    ; Original file loading address will be readily
    ; available at .Y/.X, for an immediate use with
    ; decruncher routine, e.g. with "ByteBoozer 2":
                jsr decrunch
    ;----------------------------------------------
                ; ...
    ;----------------------------------------------
    datafile    .dp "data,prg"
    datafile_length = *-datafile
    ;----------------------------------------------
    #include "decruncher.src"
    ;----------------------------------------------
                *= IDE64_SEEK_LOADER_START_ADDRESS
    .binclude "loader.prg",2
    ;----------------------------------------------

COPYRIGHT AND LICENCE
---------------------

Copyright (C) 2018 by Paweł Król.

This software is distributed under the terms of the MIT license. See [LICENSE](LICENSE.md) for more information.

PLEASE NOTE THAT IT COMES WITHOUT A WARRANTY OF ANY KIND!
