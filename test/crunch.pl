#!/usr/bin/perl

use strict;
use warnings;

use Archive::ByteBoozer2 qw/crunch rcrunch/;
use Data::Dumper;
use File::Basename;
use IO::File;

my ($in, $out, $relocate_output_up_to) = @ARGV;

my $basename = basename($in);
if (defined $relocate_output_up_to) {
  rcrunch($in, $relocate_output_up_to);
}
else {
  crunch($in);
}
my $crunched = qq{${in}.b2};
printf qq{'%s' -> '%s'\n}, $crunched, $out;
rename $crunched, $out;
# TODO: "diskimage.c" is buggy, thus we *have to* subtract an additional $0100 from the initial loading address
#       to make sure that source code of the disk magazine does not get overwritten by too much of loaded file!
# hack_relocated_output($out, -0x0100);

sub hack_relocated_output {
    my ($filename, $shift_loading_address) = @_;

    my $fh = IO::File->new($filename, q{r});
    binmode $fh, q{:bytes};
    my $filedata;
    sysread $fh, $filedata, 0xffff;

    my ($lo, $hi) = map { ord } split //, substr $filedata, 0, 2;
    my $new_loading_address = $hi * 0x0100 + $lo + $shift_loading_address;
    my $new_lo = chr ($new_loading_address & 0x00ff);
    my $new_hi = chr (($new_loading_address & 0xff00) >> 0x08);

    substr $filedata, 0, 2, join '', ($new_lo, $new_hi);

    $fh = IO::File->new($filename, q{w});
    binmode $fh, q{:bytes};
    syswrite $fh, $filedata, length $filedata;
    $fh->close();

    return;
}
