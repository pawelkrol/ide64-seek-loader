#!/usr/bin/perl

use strict;
use warnings;

use Cwd 'abs_path';
use Data::Dumper;
use File::Slurp;
use File::Temp qw(tempfile);
use FindBin qw($Bin);
use Readonly;

Readonly our $DREAMASS => 'dreamass -me 10 -mw 10 -v -Wall';

my ($start_address, $loader_prg, $data_prg, @input_files) = @ARGV;

my $loader_inc = "$Bin/ide64-seek-loader.inc";
die "Aborting... File already exists: $loader_inc (must not exist)" if -e $loader_inc;

my $offsets_inc = "$Bin/ide64-seek-offsets.inc";
die "Aborting... File already exists: $offsets_inc (must not exist)" if -e $offsets_inc;

$loader_prg = abs_path($loader_prg) if $loader_prg !~ m!^/!;

my @offsets = (0x0000);
my @sizes;
my $data;

for my $file (@input_files) {
  my $bin = read_file($file, { binmode => ':raw' });
  $data .= $bin;
  my $size = length ($bin);
  push @offsets, $offsets[-1] + $size;
  push @sizes, $size - 2;
}

pop @offsets;

my @offsets_1 = map { $_ & 0x000000ff } @offsets;
my @offsets_2 = map { ($_ & 0x0000ff00) >> 8 } @offsets;
my @offsets_3 = map { ($_ & 0x00ff0000) >> 16 } @offsets;
my @offsets_4 = map { ($_ & 0xff000000) >> 24 } @offsets;

my $offsets;

add_offsets(\$offsets, \@offsets_1, '_1');
add_offsets(\$offsets, \@offsets_2, '_2');
add_offsets(\$offsets, \@offsets_3, '_3');
add_offsets(\$offsets, \@offsets_4, '_4');

add_sizes(\$offsets, \@sizes, '_lo', '<');
add_sizes(\$offsets, \@sizes, '_hi', '>');

write_file($offsets_inc, { binmode => ':raw' }, $offsets);

if ($start_address =~ m/^0x([0-9a-f]{1,4})$/i) {
  $start_address = sprintf '$%04x', hex $1;
}
else {
  die "Aborting... Invalid start address: $start_address (must be a hex number)";
}

my $loader = "IDE64_SEEK_LOADER_START_ADDRESS = $start_address\n";

write_file($loader_inc, { binmode => ':raw' }, $loader);

system "cd $Bin; $DREAMASS -o $loader_prg ide64-seek-loader.src";

write_file($data_prg, { binmode => ':raw' }, $data);

unlink $loader_inc;
unlink $offsets_inc;

sub add_offsets {
  my ($output, $values, $suffix) = @_;
  ${$output} .= sprintf "offsets%s .db %s\n", $suffix, join ',', map { sprintf '$%02x', $_ } @{$values};
}

sub add_sizes {
  my ($output, $values, $suffix, $vector) = @_;
  ${$output} .= sprintf "sizes%s .db %s\n", $suffix, join ',', map { sprintf '%s$%04x', $vector, $_ } @{$values};
}